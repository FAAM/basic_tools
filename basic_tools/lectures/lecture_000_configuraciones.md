# Configuraciones


## Git

Puede descargar el instalador en la [página oficial](https://git-scm.com/downloads) (escoger el sistema operativo correspondiente).

Para validar si tu instalación fue correcta, debes ejecutar en la terminal:
 ```
 git --version
 ```
Usuarios de Windows que no agregaron Git al `PATH` tendrán que utilizar la terminal `Git Bash`.


## GitHub

* Utilizando tu correo institucional puedes registrarte a través de [GitHub Student Developer Pack](https://education.github.com/pack), con el cual puedes acceder a repositorios privados, entre otras cosas. En caso contrario, puedes crear una cuenta directamente en el [sitio oficial](https://github.com/).


## Virtual Enviroments

### Conda

* Seguir la instalación regular de desde la [documentación oficial](https://docs.conda.io/projects/conda/en/latest/user-guide/install/) según tu sistema operativo.
* Instalar **Miniconda**
    - **Windows**: En su menú de inicio tendrán dos nuevos programas **Anaconda Prompt** y **Anaconda Powershell Prompt**, pueden ocupar cualquiera. Personalmente prefiero `Powershell`.
    - **Linux**: En la instalación se recomienda agregar `conda` al `PATH`. Si cada vez que inicias una terminal vez el texto `(base)` al comienzo, debes ejecutar `conda config --set auto_activate_base false` y luego cada vez que quieras utilizar conda debes ejecutar `conda activate`.
    
### Poetry 

Poetry proporciona un instalador personalizado que instalará poetry aislado del resto de su sistema al vender sus dependencias. Esta es la forma recomendada de instalar poesía.

* **osx/linux/bashonwindows**
```terminal
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
```

* **windows powershell**
```terminal
(Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py -UseBasicParsing).Content | python -
```

## Pycharm

Se recomienda ocupar el IDE de Pycharm. Puede descargar el instalador en la [página oficial](https://www.jetbrains.com/pycharm/download/#section=linux) (escoger el sistema operativo correspondiente). 
Existen dos versiones: **Professional** (Pagado) y **Comunnity** (Gratis). Para efectos de este tutorial con la versión **Comunnity** será suficiente.

```{note}
No es estrictamente necesario instalarlo, pero facilita bastante aspecto códificación con Python y cosas de versionamiento.
```

## Entorno de trabajo

* Clonar el repositorio oficial de este tutorial: 
```
git clone https://gitlab.com/FAAM/basic_tools
```
en alguna carpeta que estimes conveniente.


* Para crear el entorno virtual ejecuta el siguiente comando
    * **Conda**:
        * **Crear ambiente virtual**: `conda env create -f environment.yml --yes`
        * **Activar ambiente virtual**: `conda activate NAME_ENV`

    * **Poetry**:
        * **Crear ambiente virtual**: `poetry install`
        * **Activar ambiente virtual**: `poetry shell`


* Felicitaciones, tienes todo lo necesario para triunfar en este curso!


