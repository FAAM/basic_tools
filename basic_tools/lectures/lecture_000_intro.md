# Introducción

<a id='c1'></a>
## Sistema Operativo


<img src="https://www.howtogeek.com/thumbcache/2/200/8b2cb8c7c5fc73604d66fd5f0c38be7a/wp-content/uploads/2018/08/img_5b68e80f77e33.png" alt="" align="center"/>


* Personalmente recomiendo **Linux**, en particular distribuciones como Ubuntu, Mint o Fedora por su facilidad a la hora de instalar.
* En ocasiones las implementaciones en **Windows** no están completamente integradas e inclusive en ocasiones no están disponibles.
    - Una alternativa es [**Windows Subsystem for Linux**](https://docs.microsoft.com/en-us/windows/wsl/about), pero lamentablemente no se asegura un 100% de compatibilidad.
* En el caso que poseas un equipo con **macOS** no debería haber problema.

### Interfaz de Línea de Comandos (*Command Line Interface* / CLI)

* Es un método que permite a los usuarios interactuar con algún programa informático por medio de líneas de texto.
* Típicamente se hace uso de una terminal/*shell* (ver imagen).
* En el día a día dentro de la oficina facilita flujo de trabajo.
* Permite moverse entre manipular directorios y ficheros, instalar/actualizar herramientas, aplicaciones, softwares, etc.

<img src="https://upload.wikimedia.org/wikipedia/commons/2/29/Linux_command-line._Bash._GNOME_Terminal._screenshot.png" alt="" align="center"/>

*Screenshot of a sample bash session in GNOME Terminal 3, Fedora 15. [Wikipedia](https://en.wikipedia.org/wiki/Command-line_interface)* 

<a id='c3'></a>
## Python

<img src="https://wordsofthislife.com/wp-content/uploads/2013/11/python_icon-1.png" alt="" width="300" height="300" align="center"/>

[Python](https://www.python.org/) es un lenguaje de programación interpretado cuya filosofía hace hincapié en la legibilidad de su código.

Se trata de un lenguaje de programación multiparadigma, ya que soporta orientación a objetos, programación imperativa y, en menor medida, programación funcional. Es un lenguaje interpretado, dinámico y multiplataforma.

Las principales librerías científicas a instalar y que ocuparemos durante el curso son:

* [Numpy](http://www.numpy.org/): Computación científica.
* [Pandas](https://pandas.pydata.org/): Análisis de datos.
* [Matplotlib](https://matplotlib.org/): Visualización.
* [Scikit-Learn](http://scikit-learn.org/stable/): Machine Learning

Durante el curso se ocuparán más librerías a modo de complementación (ejemplo, scipy, seaborn, statsmodels ,etc.)

<a id='c2'></a>
### Entorno Virtual

<img src="https://files.virgool.io/upload/users/63719/posts/bko9k535kg6q/j1xiuolo2c9c.png" alt="" align="center"/>


__Problemas recurrentes:__
- Dependencias de librerías (*packages*) incompatibles.
- Dificultad a la hora de compartir y reproducir resultados, e.g. no conocer las versiones de las librerías instaladas.
- Tener una máquina virtual para cada desarrollo es tedioso y costoso.
- Miedo constante a instalar algo nuevo y tu script vuelva a funcionar.

**Solución** 

Aislar el desarrollo con tal de mejorar la compatibilidad y reproducibilidad de resultados. 

**Para el curso (es recomendable)**

![Conda](https://conda.io/docs/_images/conda_logo.svg)

*Package, dependency and environment management for any language—Python, R, Ruby, Lua, Scala, Java, JavaScript, C/ C++, FORTRAN.* [(Link)](https://conda.io/docs/)

**¿Por qué Conda?**

* Open Source
* Gestor de librerías __y__ entornos virtuales. 
* Compatible con Linux, Windows y macOS.
* Es agnóstico al lenguaje de programación (inicialmente fue desarrollado para Python).
* Es de fácil instalación y uso.

**Otras alternativas**

* `pip + virtualenv`: el primero es el gestor favorito de librerías de Python y el segundo es un gestos de entornos virtuales, el contra es que es exclusivo de Python.
* `Pipenv` o  `Poetry`: librerías enfocadas al manejo de dependencias (muy recomendables!) 


## Entorno de desarrollo integrado

<img src="https://cdn-images.visual-paradigm.com/features/v12/ide-image.png" alt="" width="300" height="300" align="center"/>

* Un [entorno de desarrollo integrado](https://es.wikipedia.org/wiki/Entorno_de_desarrollo_integrado), en inglés **Integrated Development Environment (IDE)**, es una aplicación informática que proporciona servicios integrales para facilitarle al desarrollador o programador el desarrollo de software.

* Normalmente, un IDE consiste de un editor de código fuente, herramientas de construcción automáticas y un depurador. La mayoría de los IDE tienen auto-completado inteligente de código (IntelliSense). 

* Algunos IDE contienen un compilador, un intérprete, o ambos, tales como NetBeans y Eclipse; otros no, tales como SharpDevelop y Lazarus.

Existen varios IDE populares que sirven para varios lenguajes de programaci+on. En python, el más recomendable es [Pycharm](https://www.jetbrains.com/pycharm/?gclid=Cj0KCQjw-uH6BRDQARIsAI3I-Uc0HFf8ll0EZRnc6FCFaozMsnxkCiciIok-iRMyfN1xHB9vaArGw6IaAnV7EALw_wcB).

### Pycharm

<img src="https://resources.jetbrains.com/storage/products/pycharm/img/meta/pycharm_logo_300x300.png" alt="" width="200" height="200" align="center"/>

PyCharm es un IDE para desarrolladores profesionales. Fue creado por JetBrains, una empresa conocida por crear excelentes herramientas de desarrollo de software.

Hay dos versiones de PyCharm:

* **Community**: versión gratuita de código abierto, ligera, buena para Python y desarrollo científico
* **Professional**: versión de pago, IDE con todas las funciones con soporte para desarrollo web también

### Observación

Se recomienda que puedan descargar Pycharm (en su versión gratuita) para poder familiarizarse con este tipo de herramientas, aunque el curso está orientado a trabajar sobre la terminal y con jupyter notebook.

<a id='c4'></a>
## Project Jupyter 

[Project Jupyter](https://jupyter.org/index.html) exists to develop open-source software, open-standards, and services for interactive computing across dozens of programming languages.*

<img src="https://2.bp.blogspot.com/-Q23VBETHLS0/WN_lgpxinkI/AAAAAAAAA-k/f3DJQfBre0QD5rwMWmGIGhBGjU40MTAxQCLcB/s1600/jupyter.png" alt="" width="360" height="360" align="center"/>

### Jupyter Notebook

Es una aplicación web que permite crear y compartir documentos que contienen código, ecuaciones, visualizaciones y texto. Entre sus usos se encuentra:

* Limpieza de datos
* Transformación de datos
* Simulaciones numéricas
* Modelamiendo Estadístico
* Visualización de Datos
* Machine Learning
* Mucho más.

### Jupyter Lab

* Es la siguiente generación de la interfaz de usuario de *Project Jupyter*.
* Similar a Jupyter Notebook cuenta con la facilidad de editar archivos .ipynb (notebooks) y heramientas como una terminal, editor de texto, explorador de archivos, etc.
* Eventualmente Jupyter Lab reemplazará a Jupyter Notebok (aunque la versión estable fue liberada hace algunos meses).
* Cuenta con una serie de extensiones que puedes instalar (y desarrollar inclurisve.
* Más información en: https://github.com/jupyterlab/jupyterlab-demo


Puedes probar Jupyter Lab con solo dos clicks!

1. Ingresar a este link: https://github.com/jupyterlab/jupyterlab-demo
2. Hacer click en el icono de binder:  ![Binder](https://mybinder.org/badge_logo.svg)

### Otros Proyectos

Entre los más conocidos se encuentran:

* [JupyterHub](https://jupyterhub.readthedocs.io/): Distribuir Jupyter Noterbooks a múltiples usuarios.
* [nbviewer](https://nbviewer.jupyter.org/): Compartir Jupyter Notebooks.
* [Jupyter Book](https://jupyterbook.org/): Construir y publicar libros de tópicos computacionales.
* [Jupyter Docker Stacks](https://jupyter-docker-stacks.readthedocs.io/): Imágenes de Jupyter para utilizar en Docker.

<a id='c5'></a>
## Versionamiento de Código

<img src="https://i1.wp.com/help.lieberlieber.com/download/attachments/32342017/image2019-7-2_14-53-19.png?w=1080&ssl=1" width="480" height="240" align="center"/>

* Permite compartir el código fuente de nuestros desarrollos y a la vez mantener un registro de los cambios por los que va pasando.

* Herramienta más importante y fundamental dentro del desarrollo.
* Tipos de versionadores de código:
 * [Sistemas Centralizados](https://sites.google.com/site/practicadesarrollosoft/temario/sistemas-de-versionado-de-cdigo/sistemas-de-versionado-de-cdigo-centralizados): Son los más "tradicionales", por ejemplo SVN, CVS, etc.
 * [Sistemas Distribuidos](https://sites.google.com/site/practicadesarrollosoft/temario/sistemas-de-versionado-de-cdigo/sistemas-de-versionado-de-cdigo-distribuidos): son los que están en auge actualmente como: Git, Mercurial, Bazaar, etc.




### Git

<img src="https://static.platzi.com/media/user_upload/Beginners_guide_setting_up-git-a4bacd39-5be0-4ae4-a956-5117c18efa94.jpg" alt="" width="240" height="240" align="center"/>

_[__Git__](https://git-scm.com/) is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency._



Es importante comprender que _Git_ es la herramienta que permite versionar tus proyectos, sin embargo, a la hora de querer aprovechar más funcionalidades, como compartir o sincronizar tus trabajos se hace necesario utilizar servicios externos. Los más famosos son:

* GitHub
* GitLab
* Bitbucket

Piensa lo siguiente, cualquiera podría implementar un correo electrónico entre dos computadoras conectadas entre ellas por LAN pero no conectadas a Internet. Sin embargo la gente utiliza servicios como Gmail, Outlook, etc. con tal de aprovechar de mejor manera las funcionalidades que ofrece la tecnología del correo electrónico. Esta es una analogía perfecta entre las diferencias de Git y los servicios como GitHub o GitLab.

### GitHub

<img src="https://miro.medium.com/max/1125/1*wotzQboYWAfaj-7bvGNIkQ.png" alt="" width="350" height="160" align="center"/>



_[GitHub](https://github.com/) is a development platform inspired by the way you work. From open source to business, you can host and review code, manage projects, and build software alongside 30 million developers._

### Gitlab

<img src="https://blog.desafiolatam.com/wp-content/uploads/2017/10/gitlab-cover.png" alt="" width="350" height="160" align="center"/>



_[Gitlab](https://gitlab.com/) is an open source end-to-end software development platform with built-in version control, issue tracking, code review, CI/CD, and more. Self-host GitLab on your own servers, in a container, or on a cloud provider._

### Bitbucket

<img src="https://wptavern.com/wp-content/uploads/2016/10/bitbucket-logo.png" alt="" width="350" height="160" align="center"/>



_[Bitbucket](https://bitbucket.org/product/) is more than just Git code management. Bitbucket gives teams one place to plan projects, collaborate on code, test, and deploy._

. 

### Resumen

* Sistema operativo: Cualquiera, sin embargo se recomiendan alternativas basadas en Unix.
* Lenguaje de programación: Python
* Entorno virtual: Conda, preferentemetne a través de miniconda.
* Entorno de trabajo: Jupyter Lab.
* Versionamiento: Git & GitHub.
