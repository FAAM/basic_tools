# Herramientas básicas para Data Scientists



<a href="https://fralfaro.gitlab.io/basic_tools/"><img alt="Link a la Documentación" src="https://jupyterbook.org/badge.svg"></a>
[![pipeline status](https://gitlab.com/fralfaro/basic_tools/badges/master/pipeline.svg)](https://gitlab.com/fralfaro/basic_tools/-/commits/master)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/fralfaro%2Fbasic_tools/HEAD)


## Contenidos temáticos
- Linux
- Git
- IDE’s
- Python
- Jupyter
- Google Colab






