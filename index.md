# Home
Herramientas básicas para Data Scientists

## Material

El material está disponible en el siguiente [repositorio](https://gitlab.com/fralfaro/basic_tools), para obtener el código de fuente basta con que ejecutes el siguiente comando:

```
https://gitlab.com/fralfaro/basic_tools
```

## Contenidos

```{tableofcontents}
```